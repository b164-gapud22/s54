let collection = [];


// Write the queue functions below.
	

// 1. Output all the elements of the queue


	function print() {
		return collection
	}
	


// 2. Adds element to the rear of the queue


	function enqueue(element) {
		const addToArray = collection.length;

		collection[addToArray] = element;

		return collection;

	}



// 3. Removes element from the front of the queue


	function dequeue() {

		collection.shift();
		return collection;
	}
	


// 4. Show element at the front

	function front() {
		const frontElement = collection[0];

		return frontElement;
	}


// 5. Show the total number of elements

	function size() {

		const totalElements = collection.length;

		return totalElements;

	}



// 6. Outputs a Boolean value describing whether queue is empty or not


 function isEmpty() {

 	if(collection.length != 0){
 		return false;
 	}
 };



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};
